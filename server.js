var mongojs = require('mongojs');
var express = require('express');
var app = express();

var collections = ["litusr", "litlocs"]
var db = mongojs('litdata',collections);

var bodyparser = require('body-parser');

app.use(express.static(__dirname + "/public"));
app.use(bodyparser.json());


//##############################
//      BEGIN APP METHODS
//##############################

//Post and Add record to the database (required body-parser addon)
//Repost


// --- BEGIN BASIC INSERT ---
//// Depreciated: BASIC INSERT 
//app.post('/litusr', function (req, res) {
//  //console.log('Got to post');
//    db.litusr.insert(req.body, function(err, doc) {
//    res.json(doc);
//  });    
//}); //--- END OF post/INSERT
// --- END BASIC INSERT ---

// --- BEGIN USERUPSERT ---
// USED TO ADD or UPDATE last logged into app
app.post('/api/litusr/:user_id', function (req, res) {
    //console.log('Got to Upsert');
    var x_user_id = req.params.user_id;
    db.litusr.update({user_id:x_user_id}, req.body, {upsert: true}, function(err, doc) {
    res.json(doc);
    
    });
}); //--- END OF post/UPINSERT
// --- END USERUPSERT ---

// --- BEGIN USER FIND ---
// Find all users 
app.get('/api/litusr',function(req,res){ 
    db.litusr.find(function(err,docs){
     //   console.log(docs);
        res.json(docs);
    }); 
});
// --- END LOC FIND ---


// --- BEGIN LOC INSERT ---
//  BASIC INSERT 
app.post('/api/litloc', function (req, res) {
    db.litlocs.insert(req.body, function(err, doc) {
    res.json(doc);
  });    
}); //--- END OF post/INSERT
// --- END LOC INSERT ---

// --- BEGIN LOC FIND ---
// Find all user locations
app.get('/api/litloc/:user_id',function(req,res){ 
    db.litlocs.find({user_id: req.params.user_id},function(err,docs){
        res.json(docs);
    }); 
});
// --- END LOC FIND ---

// --- BEGIN LOC DELETE ---
//  BASIC INSERT 
app.post('/api/litlocdelete/:user_id', function (req, res) {
    var targetmarkerid = req.body.markerid;
    console.log(targetmarkerid);
    db.litlocs.remove({user_id: req.params.user_id,markerid: targetmarkerid},function(err,docs){
        res.json(docs);
    });  
    
}); //--- END OF post/INSERT
// --- END LOC DELETE ---


//##############################
//      END APP METHODS
//##############################


app.listen(3000);
console.log("LIT Server Started, port 3000.");

