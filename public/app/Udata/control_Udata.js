
angular.module('app').controller('UdataController', function($scope, $timeout, mapboxService,litsessionData,auth,store,$location,$http) {
   
    var storedprofile = store.get('profile');
    
   //$scope.litpoints = litsessionData.getDataPoints();
   // console.log($scope.litpoints);
    
    // 1. --- UPON LOGIN UPDATE OR INSERT USER ---
    $scope.litprofile = {       
          user_id: storedprofile.user_id
        , last_name: storedprofile.family_name
        , first_name: storedprofile.given_name
        , email: storedprofile.email
        ,img:" https://graph.facebook.com/"+storedprofile.identities[0].user_id+"/picture?width=100&height=100"
        , lastlog:new Date().toString()
    };       
    //console.log(storedprofile);
    
    // 1. ----------------------------------------

//##########  Begin Define Methods ###########  
    $scope.addUpsertProfile = function() {    
        $http.post('/api/litusr/'+$scope.litprofile.user_id, $scope.litprofile).success(function(response) {
        });
    };
    
    $scope.addLoc = function(locinfo) {
        $http.post('/api/litloc', locinfo).success(function(response) {
        });
    };
    
    $scope.getLocs = function(usr){
        $http.get('/api/litloc/'+usr).success(function(response) {
            console.log(response);
        });
    }; 
    
    $scope.delLocs = function(usr,mkr){
        $http.post('/api/litlocdelete/'+usr,mkr).success(function(response) {
            console.log(response);
        });
    }; 
//##########  End Define Methods ###########  
    
          
//##########  Begin Run Methods ###########   
    
    $scope.addUpsertProfile();
        
//##########  End Run Methods #############
    
    $scope.litpoints = litsessionData.getDataPoints(); 
    
    $scope.getLocs($scope.litprofile.user_id);
   // $scope.delLocs($scope.litprofile.user_id,{markerid:3});
    
//    if($scope.litpoints.length == 1){
//        //insert points
//         var newlocA = {
//			name: "Test Text A",
//			times: new Date().toString(),
//			icon: 'triangle',
//			coords: {
//			     lat:33.64749293739745
//                ,lng:-117.84853935241698
//			},
//             markerid: 2,
//			 litkind:"whiteUKN"
//		}; 
//        newlocA.user_id = storedprofile.user_id;
//        $scope.addLoc(newlocA);
//        
//        var newlocB = {
//			name: "Test Text B",
//			times: new Date().toString(),
//			icon: 'triangle',
//			coords: {
//			     lat:33.663639305213515
//                ,lng:-117.86441802978514
//			},
//             markerid: 3,
//			 litkind:"whiteUKN"
//		}; 
//        newlocB.user_id = storedprofile.user_id;
//        $scope.addLoc(newlocB);
//        
//        //console.log(newlocA);
//    }
   
  
});//--- END CONTROLLER ---



