
angular.module('app').controller('mapController', function($scope, $timeout, mapboxService,litsessionData,auth,store,$location,$http,$uibModal) {
    
    
    
    $scope.litlocations = [];    
    $scope.litfriends = [];
    $scope.litfriendlocations = [];
    
   
    
    //##########  Begin Define Methods ###########  
    $scope.addUpsertProfile = function() {    
        $http.post('/api/litusr/'+$scope.litprofile.user_id, $scope.litprofile).success(function(response) {
        });
    };
    
    $scope.addLoc = function(locinfo) {
        $http.post('/api/litloc', locinfo).success(function(response) {
        });
    };
    
    $scope.getLocs = function(usr){
        $http.get('/api/litloc/'+usr).success(function(response) {
            //console.log(response);
            for (index = 0; index < response.length; ++index) {
               // console.log(a[index]);
                 $scope.litlocations.push(response[index]);
            }
        });
    }; 
    
     $scope.getFriendLocs = function(usr){
        $http.get('/api/litloc/'+usr).success(function(response) {
            //console.log(response);
            for (index = 0; index < response.length; ++index) {
               // console.log(a[index]);
                 $scope.litfriendlocations.push(response[index]);
            }
        });
    }; 
    
    $scope.getfriends = function(usr){
        $http.get('/api/litusr').success(function(response) {
            
            for (index = 0; index < response.length; ++index) {
                 var xfriend = response[index];
                 if(xfriend.user_id != $scope.litprofile.user_id){
                     $scope.litfriends.push(xfriend);
                 }
                 
                 
            }
           // console.log($scope.litfriends);
        
        });
    }; 
    
    
    $scope.delLocs = function(usr,mkr){
        $http.post('/api/litlocdelete/'+usr,mkr).success(function(response) {
            console.log(response);
        });
    }; 
    
    
    $scope.delLocsandindex = function(usr,mkr,idx){
      $scope.delLocs(usr,mkr);
      $scope.litlocations.splice(idx,1);
    };
    
//##########  End Define Methods ###########  
    var storedprofile = store.get('profile');
   
    $scope.litprofile = {       
          user_id: storedprofile.user_id
        , last_name: storedprofile.family_name
        , first_name: storedprofile.given_name
        , email: storedprofile.email
        , img:" https://graph.facebook.com/"+storedprofile.identities[0].user_id+"/picture?width=100&height=100"
        , lastlog:new Date().toString()
     };  
    
    $scope.addUpsertProfile();
    $scope.getLocs($scope.litprofile.user_id);
     //console.log($scope.litlocations );
    
    
    $scope.getfriends($scope.litprofile.user_id);
     //console.log($scope.litfriends );
    
    $scope.inputvis = 'none';
    $scope.auth = auth;
    //console.log(auth);
    var map;
    var savedcoords = litsessionData.get();
    $scope.addpoints = false;
    $scope.ptsbtnlabel = "Add Point";

    $scope.CurrentLat = savedcoords.xlat;
    $scope.CurrentLng = savedcoords.xlng;
    
	$scope.clr = '#b30000';	
	//$scope.iconkind = 'whiteUKN';
    $scope.iconkind = 'cls';

    $scope.newloc = {
			name: "LitApp Check-in",
			times: new Date().toString(),
			icon: 'triangle',
			coords: {
                lat: $scope.CurrentLat, 
			    lng: $scope.CurrentLng
			},
             markerid: 0
		};
    
    var dot1   = {
			name: "LitApp Check-in",
			times: new Date().toString(),
			icon: 'triangle',
			coords: {
                lat: $scope.CurrentLat, 
			    lng: $scope.CurrentLng
			},
             markerid: 0,
			  litkind:'self'
   //          litkind:'cls'
		};
    
    $scope.litlocations.push(dot1);
    $scope.newloc.name = "";
    
    litsessionData.setDataPoints($scope.litlocations);
    
    var litloccount = $scope.litlocations.length - 1;
    var litloclast = 0  
        if(litloccount >= 0){
           litloclast  =  $scope.litlocations[litloccount].markerid;
         }

    //$scope.getLocs($scope.litprofile.user_id);
    
	mapboxService.init({ accessToken: '<NEED MAPBOX TOKEN>' });
	    	
	$timeout(function() {
     
        
      var lastnum = mapboxService.getMapInstances().length;
      if(lastnum > 1){ mapboxService.getMapInstances().shift();}
        
      map = mapboxService.getMapInstances()[0];

       
       $scope.$apply();
      
        
	  map.on('click', function(e) {		 
	     litloclast = litloclast+1;
          
		 var newloc2 = {     
			name: $scope.newloc.name,
			times: new Date().toString(),
			icon: 'triangle',
			coords: {
			  lat: e.latlng.lat, 
			  lng: e.latlng.lng
			},
             markerid: new Date().getUTCMilliseconds(),
			 litkind:$scope.iconkind
		}; 
        
        $scope.inputvis = 'none';
		
		if($scope.addpoints)
		{
	    $scope.litlocations.push(newloc2);
        litsessionData.setDataPoints($scope.litlocations);
        $scope.addtoggle();
        $scope.$apply();
        newloc2.user_id = storedprofile.user_id;
        $scope.addLoc(newloc2);
		}
	  });
	  
    }, 100); //end TIMEOUT
//----
   

	$scope.addtoggle = function () {
         $scope.inputvis = 'inline';
        $scope.addpoints = !$scope.addpoints;
        if($scope.addpoints == false){$scope.ptsbtnlabel = "Add Point";
                                     $scope.inputvis = 'none';
                                     }
        else{$scope.ptsbtnlabel = "Cancel";}
        
    };
	
    
    
	$scope.setcolor = function (color) {
       $scope.clr = color;
    }
	
    $scope.removemark = function(xmarker){
        for(var x in $scope.litlocations)
            {
                if($scope.litlocations[x].xmarker == xmarker)
                {
                    $scope.litlocations[x].splice(x, 1);
                    $scope.$apply();
                }       
            }
    }
    //user_id
    $scope.logout = function() {
      auth.signout();
      store.remove('profile');
      store.remove('token');
      $location.path('/login');
          
    }


   $scope.friendclicked = function(usr){
       console.log("ping");
       $scope.getFriendLocs(usr);
   };

  });



