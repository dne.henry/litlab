


angular.module('app', ['angular-mapbox','ui.bootstrap','ngRoute', 'auth0',
  'angular-storage',
  'angular-jwt',
  'colorpicker.module']);



angular.module('app').config(
    function($routeProvider,authProvider) { //,$httpProvider
		$routeProvider

			// route for the first page
			.when('/', {
				templateUrl : 'app/login/page_login.html',
				controller  : 'loginController'
			})
         
           // route for the home page
			.when('/mappage', {
				templateUrl : 'app/mappage/page_map.html',
				controller  : 'mapController',
                requiresLogin: true
			})
        
          // route for the Udata page
			.when('/Udata', {
				templateUrl : 'app/Udata/page_data.html',
				controller  : 'UdataController',
			})

        

			// route for the about page
			.when('/login', {
				templateUrl : 'app/login/page_login.html',
				controller  : 'loginController'
			})
        
        .otherwise({
        redirectTo: '/'
      });
  

        ;//-- END ROUTEPROVIDER
        
        authProvider.init({
            domain: 'litapp.auth0.com',
            clientID: 'bGgu7zOybgTfgLaY93EAok1aOoXfAmcE',
           // callbackURL: location.href,
            callbackURL: '/mappage',
            // Here include the URL to redirect to if the user tries to access a resource when not authenticated.
            loginUrl: '/login'
          });
        
   //     $httpProvider.defaults.useXDomain = true;
   //        delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
    
	);//-- END CONFIG

angular.module('app').run(function(auth) {
  // This hooks all auth events to check everything as soon as the app starts
  auth.hookEvents();
});

angular.module('app').factory('litsessionData', function() {
 var savedData = [];
 var savedDataPoints = [];
 var picsrc;
 var name;
 
function setname(data) {
   name = data;
 }
 
 function getname() {
   return name;
 }    
    
 function setpicsrc(data) {
   picsrc = data;
 }
 
 function getpicsrc() {
   return picsrc;
 }
    
 function set(data) {
   savedData = data;
 }
 function get() {
  return savedData;
 }
    
function setDataPoints(data){
    savedDataPoints = data;
}    

function getDataPoints(){
    return savedDataPoints;
}
    
    
 return {
  set: set,
  get: get,
  setpicsrc:setpicsrc,
  getpicsrc:getpicsrc,
  setname:setname,
  getname:getname,
  setDataPoints:setDataPoints,
  getDataPoints:getDataPoints
 }

});




