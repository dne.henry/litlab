angular.module('app').controller('loginController', ['$scope', '$http', 'auth', 'store', '$location','$timeout','litsessionData',
function ($scope, $http, auth, store, $location, $timeout,litsessionData) {

    $scope.login = function () {
        auth.signin({
//            connections: [ 'facebook']
            
        }, function (profile, token) {
        // Success callback
            store.set('profile', profile);
            store.set('token', token);
            $location.path('/mappage');
        }, function () {
        // Error callback
        });
    } //END LOGIN FUNCTION
    

                                                     
    var options = {
    enableHighAccuracy: true,
    timeout: 15000,
    maximumAge: 0
  };
    
    function success(pos) {
    var crd = pos.coords;

    //console.log(crd);
    
        $scope.CurrentLat = crd.latitude;
        $scope.CurrentLat = crd.longitude;
        
        var datatosave = {
            xlat:crd.latitude,xlng:crd.longitude
        };
        //console.log(datatosave);
        litsessionData.set(datatosave);
        
        var loginparentElement = document.getElementById('deviceready');
        var loginlisteningElement = loginparentElement.querySelector('.listening');
        loginlisteningElement.setAttribute('style', 'display:none;');
       
        $scope.login();
    };

    function error(err) {
      console.warn('ERROR(' + err.code + '): ' + err.message);
    };
                                   
                                                     
    $timeout(function() {
        navigator.geolocation.getCurrentPosition(success, error, options);   
      
	  
    }, 15000); //end TIMEOUT                                                 
                                                     
    

}]);// -- END CONTROLLER
